properties([
  // Prune the build history down to 10 previous builds
  [$class: 'jenkins.model.BuildDiscarderProperty', strategy: [$class: 'LogRotator', numToKeepStr: '10']]
])

def getCommitHash() {
  return sh(script: 'git show --no-patch --abbrev=10 --format=%h', returnStdout: true).trim()
}

def app

pipeline {
  agent any

  stages {
    stage('Setup') {
      steps {
        sh 'yarn'
      }
    }

    stage('Run tests') {
      steps {
        sh 'yarn test:auto'
        checkstyle canComputeNew: false, canRunOnFailed: true, defaultEncoding: '', failedTotalAll: '0', healthy: '', pattern: 'checkstyle.xml', unHealthy: ''
        step([$class: 'CoberturaPublisher', autoUpdateHealth: false, autoUpdateStability: false, coberturaReportFile: 'coverage/cobertura-coverage.xml', failUnhealthy: false, failUnstable: false, lineCoverageTargets: '80.0, 80.0, 0.0', conditionalCoverageTargets: '80.0, 80.0, 0.0', maxNumberOfBuilds: 0, onlyStable: false, sourceEncoding: 'ASCII', zoomCoverageChart: false])
        step([$class: 'XUnitPublisher', testTimeMargin: '3000', thresholdMode: 1, thresholds: [[$class: 'FailedThreshold', failureNewThreshold: '', failureThreshold: '0', unstableNewThreshold: '', unstableThreshold: ''], [$class: 'SkippedThreshold', failureNewThreshold: '', failureThreshold: '', unstableNewThreshold: '', unstableThreshold: '']], tools: [[$class: 'JUnitType', deleteOutputFiles: true, failIfNotNew: true, pattern: 'test-report.xml', skipNoTestFiles: false, stopProcessingIfError: true]]])
        publishHTML([allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'coverage/', reportFiles: 'index.html', reportName: 'HTML Coverage Report', reportTitles: 'Coverage Report'])
      }
    }

    stage('Build image') {
      steps {
        sh 'yarn build'
        script {
          app = docker.build('test-app')
        }
      }
    }

    // TODO: Add some sort of test here to verify the image was built successfully
    stage('Test image') {
      steps {
        script {
          app.inside {
            sh "echo 'Tests passed'"
          }
        }
      }
    }

    // stage('Push image') {
    //   when {
    //     branch 'master'
    //   }

    //   steps {
    //     Add creditials for the docker registry
    //     withDockerRegistry([]) {
    //       script {
    //         app.push('latest')
    //       }
    //     }
    //   }
    // }
  }
}