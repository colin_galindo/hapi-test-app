FROM node:9.8.0

# Create environment variables
ENV APP=hapi-test-app
ENV PROJECT_HOME /usr/local/${APP}

# Setup node port and expose it
ENV NODE_PORT 8080
EXPOSE ${NODE_PORT}

# Copy build files to docker image
RUN mkdir -p $PROJECT_HOME
WORKDIR $PROJECT_HOME
COPY . .

# Grant entrypoint.bash executable rights
RUN chmod +x ./tools/docker/entrypoint.bash

# Set maintainer
LABEL maintainer "galindo.colin@gmail.com"

CMD ["./tools/docker/entrypoint.bash"]
