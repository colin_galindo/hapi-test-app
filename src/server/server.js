const Hapi = require('hapi');
const Path = require('path');

const server = Hapi.server({
  port: process.env.NODE_PORT || 8080,
  host: '0.0.0.0'
});

const goodOptions = {
  ops: {
    interval: 1000
  },
  reporters: {
    consoleReporter: [{
      module: 'good-squeeze',
      name: 'Squeeze',
      args: [{ log: '*', response: '*' }]
    }, {
      module: 'good-console'
    }, 'stdout'],
    opsFileReporter: [{
      module: 'good-squeeze',
      name: 'Squeeze',
      args: [{ ops: '*' }]
    }, {
      module: 'good-squeeze',
      name: 'SafeJson'
    }, {
      module: 'good-file',
      args: ['./logs/ops.log']
    }],
    resFileReporter: [{
      module: 'good-squeeze',
      name: 'Squeeze',
      args: [{ log: '*', response: '*' }]
    }, {
      module: 'good-squeeze',
      name: 'SafeJson'
    }, {
      module: 'good-file',
      args: ['./logs/response.log']
    }],
    errorFileReporter: [{
      module: 'good-squeeze',
      name: 'Squeeze',
      args: [{ errorFileReporter: '*' }]
    }, {
      module: 'good-squeeze',
      name: 'SafeJson'
    }, {
      module: 'good-file',
      args: ['./logs/error.log']
    }],
  }
};


const init = async () => {
  await server.register(require('inert'));
  await server.register({
    plugin: require('good'),
    options: goodOptions,
  });
  await server.register({
    plugin: require('hapi-pino'),
    options: {
      prettyPrint: true,
      logEvents: false
    }
  });
  
  server.route({
    method: 'GET',
    path: '/{path*}',
    handler: {
      directory: {
        path: Path.join(__dirname, '..', '..', 'build'),
        listing: false,
        index: true
      }
    }
  });
  
  await server.start();
  server.logger().info(`Dirname: ${__dirname}`);
  server.logger().info(`Server running at: ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {
  server.logger().error(err);
  process.exit(1);
});

init();