import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './universal/components/App/App';
import registerServiceWorker from './universal/utils/registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
